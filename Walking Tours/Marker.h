

#import <Foundation/Foundation.h>

@interface Marker : NSObject

@property (nonatomic) int markerId;
@property (nonatomic,strong) NSString *name;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;



@end
