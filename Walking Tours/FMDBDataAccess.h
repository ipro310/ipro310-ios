

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMResultSet.h"
#import "Utility.h"
#import "Marker.h"


@interface FMDBDataAccess : NSObject
{
    

}

-(NSMutableArray *) getMarkers: (NSString *) tour;
-(NSMutableArray *) getTourDescription: (NSString *) tour;



@end
