

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface Utility : NSObject
{
    
}

+(NSString *) getDatabasePath;
+(void) showAlert:(NSString *) title message:(NSString *) msg;

@end
