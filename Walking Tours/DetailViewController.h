//
//  DetailViewController.h
//  Walking Tours
//
//  Created by Joshua Lacey on 3/24/13.
//  Copyright (c) 2013 Joseph Bonokollie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property(nonatomic) NSString* markerName;

@end
