//
//  main.m
//  Walking Tours
//
//  Created by Joseph Bonokollie on 3/4/13.
//  Copyright (c) 2013 Joseph Bonokollie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
