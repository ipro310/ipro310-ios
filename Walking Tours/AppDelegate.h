//
//  AppDelegate.h
//  Walking Tours
//
//  Created by Joseph Bonokollie on 3/4/13.
//  Copyright (c) 2013 Joseph Bonokollie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>



@property (strong, nonatomic) UIWindow *window;

@property NSString *databaseName;

@property NSString *databasePath;


@end
