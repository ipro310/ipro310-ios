

#import "FMDBDataAccess.h"

@implementation FMDBDataAccess

-(NSMutableArray *) getMarkers: (NSString *) tour
{
    NSMutableArray *markers = [[NSMutableArray alloc] init];

    FMDatabase *db = [FMDatabase databaseWithPath:[Utility getDatabasePath]];

    [db open];
    
    NSString *query;
    
    if ([tour isEqualToString:@"combined"]) {
        query = @"SELECT * FROM points";
    }
    else{
       query = [NSString stringWithFormat:@"SELECT * FROM points WHERE tour = '%@'", tour]; 
    }

    FMResultSet *results = [db executeQuery:query];

    while([results next])
    {
        Marker *marker = [[Marker alloc] init];

        marker.markerId = [results intForColumn:@"id"];
        marker.name = [results stringForColumn:@"name"];
        marker.longitude = [results doubleForColumn:@"lng"];
        marker.latitude = [results doubleForColumn:@"lat"];

        [markers addObject:marker];
        
    }

    [db close];

    return markers;

}

-(NSMutableArray *) getTourDescription: (NSString *) tour
{
    NSMutableArray *tourinfo = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[Utility getDatabasePath]];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:[NSString stringWithFormat:@"SELECT * FROM tours WHERE name = '%@'", tour]];
    
    while([results next])
    {
        [tourinfo addObject:[results stringForColumn:@"info"]];
        [tourinfo addObject:[results stringForColumn:@"image"]];
        [tourinfo addObject:[NSString stringWithFormat:@"%d",[results intForColumn:@"quantity"]]];
        [tourinfo addObject:[NSString stringWithFormat:@"%f",[results doubleForColumn:@"distance"]]];
        [tourinfo addObject:[NSString stringWithFormat:@"%f",[results doubleForColumn:@"duration"]]];
        
    }
    
    [db close];
    
    return tourinfo;
}



@end
