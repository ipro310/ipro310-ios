//
//  FirstViewController.m
//  Walking Tours
//
//  Created by Joseph Bonokollie on 3/4/13.
//  Copyright (c) 2013 Joseph Bonokollie. All rights reserved.
//

#import "FirstViewController.h"
#import "SecondViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController


- (void)viewDidLoad
{
    
    [super viewDidLoad];
	
}

-(void)viewDidAppear:(BOOL)animated
{
    
    if (_typeofTour == 0) {
        self.navigationItem.title = @"Murals Tour";
    }
    else if (_typeofTour == 1) {
        self.navigationItem.title = @"Sites Tour";
    }
    else{
        self.navigationItem.title = @"Combined Tour";
    }
    
    FMDBDataAccess *db = [[FMDBDataAccess alloc] init];
    
    NSMutableArray *tourInfo = [db getTourDescription:self.navigationItem.title];
    
    UILabel *description = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 175)];
    
    description.text = [tourInfo objectAtIndex:0];
    description.lineBreakMode = NSLineBreakByWordWrapping;
    description.numberOfLines = 20;
    description.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:description];
    
    
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    SecondViewController *controller = (SecondViewController *) segue.destinationViewController;
    
        controller.typeofTour = self.typeofTour;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
