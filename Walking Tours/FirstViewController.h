//
//  FirstViewController.h
//  Walking Tours
//
//  Created by Joseph Bonokollie on 3/4/13.
//  Copyright (c) 2013 Joseph Bonokollie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDBDataAccess.h"

@interface FirstViewController : UIViewController

@property(nonatomic) int typeofTour;

@end


