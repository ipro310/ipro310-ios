//
//  SecondViewController.m
//  Walking Tours
//
//  Created by Joseph Bonokollie on 3/4/13.
//  Copyright (c) 2013 Joseph Bonokollie. All rights reserved.
//

#import "SecondViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "DetailViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController{
    GMSMapView *mapView_;
    NSString *tourtype;
}

- (void)loadView {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:41.906611
                                                            longitude:-87.696968
                                                                 zoom:14.3];
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.myLocationEnabled = YES;
    mapView_.delegate = self;
    self.view = mapView_;
    
    if (_typeofTour == 0) {
        tourtype = @"murals";
    }
    else if (_typeofTour == 1) {
        tourtype = @"sites";
    }
    else{
        tourtype = @"combined";
    }
    
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(id<GMSMarker>)marker {
    DetailViewController *detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
    detailViewController.markerName = mapView.selectedMarker.title;
    
    
    [self.navigationController pushViewController:detailViewController animated:YES];
   
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    FMDBDataAccess *db = [[FMDBDataAccess alloc] init];
    
    NSMutableArray *markers = [db getMarkers:tourtype];
    
    for (Marker *m in markers) {
    
        GMSMarkerOptions *mapmarker = [[GMSMarkerOptions alloc] init];
        mapmarker.position = CLLocationCoordinate2DMake(m.latitude, m.longitude);
        
        mapmarker.title = m.name;
        
        [mapView_ addMarkerWithOptions:mapmarker];
        
    }
    
    
    
    
    /*GMSMarkerOptions *flag1 = [[GMSMarkerOptions alloc] init];
    GMSMarkerOptions *flag2 = [[GMSMarkerOptions alloc] init];
    flag1.position = CLLocationCoordinate2DMake(41.90306, -87.688116);
    flag2.position = CLLocationCoordinate2DMake(41.902994,-87.697925);
    
    flag1.title = @"East Flag";
    flag1.snippet = @"Paseo Boricua's iconic eastern border";
    
    flag2.title = @"West Flag";
    flag2.snippet = @"Paseo Boricua's iconic western border";
    
    [mapView_ addMarkerWithOptions:flag1];
    [mapView_ addMarkerWithOptions:flag2];*/
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
