//
//  StartViewController.m
//  Walking Tours
//
//  Created by Joshua Lacey on 3/19/13.
//  Copyright (c) 2013 Joseph Bonokollie. All rights reserved.
//

#import "StartViewController.h"
#import "FirstViewController.h"

@interface StartViewController ()

@end

@implementation StartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Walking Tour";
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    FirstViewController *controller = (FirstViewController *)segue.destinationViewController;
    if([segue.identifier isEqualToString:@"muralButtonPush"]){
        controller.typeofTour = 0;
    }
    else if([segue.identifier isEqualToString:@"pointsButtonPush"]){
        controller.typeofTour = 1;
    }
    else{
        controller.typeofTour = 2;
    }
}

@end
